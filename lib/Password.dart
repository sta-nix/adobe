import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './Tutorial.dart';

class Password extends StatelessWidget {
  Password({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(164.0, 31.0),
            child: SizedBox(
              width: 48.0,
              child: Text(
                'logo',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(22.0, 213.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Tutorial(),
                ),
              ],
              child: Container(
                width: 335.0,
                height: 46.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffc810),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(169.0, 227.0),
            child: SizedBox(
              width: 42.0,
              child: Text(
                'Finish',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 77.0),
            child: Container(
              width: 335.0,
              height: 46.0,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 91.0),
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Enter a new password'
              ),
              autofocus: false,
              obscureText: true,
            ),            
            // child: Text(
            //   'Enter a new password',
            //   style: TextStyle(
            //     fontFamily: 'Apple SD Gothic Neo',
            //     fontSize: 15,
            //     color: const Color(0xff707070),
            //     fontWeight: FontWeight.w300,
            //   ),
            //   textAlign: TextAlign.left,
            // ),
          ),
          Transform.translate(
            offset: Offset(20.0, 145.0),
            child: Container(
              width: 335.0,
              height: 46.0,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                border: Border.all(width: 1.0, color: const Color(0xff707070)),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(32.0, 159.0),
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Re-enter new password'
              ),
              autofocus: false,
              obscureText: true,
            ),
            // child: Text(
            //   'Re-enter new password',
            //   style: TextStyle(
            //     fontFamily: 'Apple SD Gothic Neo',
            //     fontSize: 15,
            //     color: const Color(0xff707070),
            //     fontWeight: FontWeight.w300,
            //   ),
            //   textAlign: TextAlign.left,
            // ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter_svg/flutter_svg.dart';

class Component21 extends StatelessWidget {
  Component21({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SvgPicture.string(
          _shapeSVG_1f9c2752e4c34da6990e18de8145d453,
          allowDrawingOutsideViewBox: true,
        ),
      ],
    );
  }
}

const String _shapeSVG_1f9c2752e4c34da6990e18de8145d453 =
    '<svg viewBox="0.0 0.0 375.0 1.0" ><path  d="M 0 0 L 375 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

import 'package:flutter/material.dart';
import 'package:adobe/Screens/Login/components/background.dart';
import 'package:adobe/Screens/Signup/components/or_divider.dart';
import 'package:adobe/Screens/Signup/components/social_icon.dart';
import 'package:adobe/Screens/Signup/signup_screen.dart';
import 'package:adobe/components/already_have_an_account_acheck.dart';
import 'package:adobe/components/rounded_button.dart';
import 'package:adobe/components/rounded_input_field.dart';
import 'package:adobe/components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';

import '../../../Login.dart';
import '../../../home.dart';

class Body extends StatelessWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              "assets/icon/login.svg",
              height: size.height * 0.35,
            ),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "LOGIN",
              press:() {Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return home();
                                },
                              ),
                            );},
              // press: () {Navigator.of(context).pushReplacementNamed("/main");},
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/icon/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/icon/kakao-talk.svg",
                  press: () {Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return LoginKScreen();
                                },
                              ),
                            );},
                ),
                SocalIcon(
                  iconSrc: "assets/icon/google-plus.svg",
                  press: () {},
                ),
              ],
            )            
          ],
        ),
      ),
    );
  }
}

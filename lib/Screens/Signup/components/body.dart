import 'package:flutter/material.dart';
import 'package:adobe/Screens/Login/login_screen.dart';
import 'package:adobe/Screens/Signup/components/background.dart';
import 'package:adobe/Screens/Signup/components/or_divider.dart';
import 'package:adobe/Screens/Signup/components/social_icon.dart';
import 'package:adobe/components/already_have_an_account_acheck.dart';
import 'package:adobe/components/rounded_button.dart';
import 'package:adobe/components/rounded_input_field.dart';
import 'package:adobe/components/rounded_password_field.dart';
import 'package:flutter_svg/svg.dart';

import '../../../Login.dart';
import '../../../Tutorial.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              "assets/icon/signup.svg",
              height: size.height * 0.35,
            ),
            RoundedInputField(
              hintText: "Your Email",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "SIGNUP",
              press: () {Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return Tutorial();
                                },
                              ),
                            );},
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            OrDivider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SocalIcon(
                  iconSrc: "assets/icon/facebook.svg",
                  press: () {},
                ),
                SocalIcon(
                  iconSrc: "assets/icon/kakao-talk.svg",
                  press: () {Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return LoginKScreen();
                                },
                              ),
                            );},
                ),
                SocalIcon(
                  iconSrc: "assets/icon/google-plus.svg",
                  press: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

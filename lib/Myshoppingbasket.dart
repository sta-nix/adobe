import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter_svg/flutter_svg.dart';
import './Component71.dart';
import 'package:adobe_xd/page_link.dart';
import './Detailpagesample.dart';
import 'home.dart';

class Myshoppingbasket extends StatelessWidget {
  Myshoppingbasket({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(82.0, 22.0),
            child: SizedBox(
              width: 212.0,
              child: Text(
                'My shopping basket',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(115.0, 61.0),
            child: SizedBox(
              width: 146.0,
              child: Text(
                'Yami\'s Hamburger',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(25.5, 95.5),
            child: SvgPicture.string(
              _shapeSVG_c79ff47ac8e44c2d96f4f80cf1773201,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 372.0),
            child: Text(
              'Reservation quantity',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 448.0),
            child: Text(
              'Total',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 410.0),
            child: Text(
              'Packing cost',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(-1.0, 70.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, 539.0),
                  child: Component71(),
                ),
                Transform.translate(
                  offset: Offset(118.0, 560.0),
                  child: SizedBox(
                    width: 148.0,
                    child: Text(
                      '오늘의 야미 결제하기',
                      style: TextStyle(
                        fontFamily: 'Apple SD Gothic Neo',
                        fontSize: 15,
                        color: const Color(0xff707070),
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushRight,
                  duration: 0.3,
                  ease: Curves.linear,
                  pageBuilder: () => home(),
                ),
              ],
              child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(337.0, 372.0),
            child: SizedBox(
              width: 14.0,
              child: Text(
                '2',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xffff4e00),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(306.0, 409.0),
            child: SizedBox(
              width: 45.0,
              child: Text(
                '\$3.00',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xffff4e00),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(299.0, 448.0),
            child: SizedBox(
              width: 52.0,
              child: Text(
                '\$17.00',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 15,
                  color: const Color(0xffff4e00),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(26.0, 111.0),
            child:
                // Adobe XD layer: 'filipe-de-rodrigues…' (shape)
                Container(
              width: 325.0,
              height: 227.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _shapeSVG_c79ff47ac8e44c2d96f4f80cf1773201 =
    '<svg viewBox="25.5 95.5 325.0 258.0" ><path transform="translate(25.5, 95.5)" d="M 0 0 L 325 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /><path transform="translate(25.5, 353.5)" d="M 0 0 L 325 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

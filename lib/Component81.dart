import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import './Component61.dart';

class Component81 extends StatelessWidget {
  Component81({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Component61(),
        Transform.translate(
          offset: Offset(146.0, 14.0),
          child: SizedBox(
            width: 40.0,
            child: Text(
              'Login',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 15,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}

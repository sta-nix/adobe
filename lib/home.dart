import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import './miguelmaldonadoqom5MPOERIunsplash.dart';
import 'package:adobe_xd/specific_rect_clip.dart';
import './Component111.dart';
import 'package:adobe_xd/page_link.dart';
import './Productthumbnailsample.dart';
import './Component141.dart';
import 'hscroll.dart';
import 'widgets/app_drawer.dart';

class home extends StatelessWidget {
  home({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(16.0, 460.0),
            child: SpecificRectClip(
              rect: Rect.fromLTWH(0, 0, 796, 94),
              child: UnconstrainedBox(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 795,
                  height: 94,
                  child: GridView.count(
                    primary: false,
                    padding: EdgeInsets.all(0),
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 5,
                    childAspectRatio: 1.5212765957446808,
                    children: [
                      {
                        'fill': const AssetImage('assets/images/miguel-maldonado-qom5MPOER-I-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                    ].map((map) {
                      final fill = map['fill'];
                      return Transform.translate(
                        offset: Offset(-16.0, -490.0),
                        child: Stack(
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(16.0, 490.0),
                              child:
                                  // Adobe XD layer: 'miguel-maldonado-qo…' (component)
                                  Stack(
                                children: <Widget>[
                                  // Adobe XD layer: 'emiliano-vittoriosi…' (shape)
                                  Container(
                                    width: 143.0,
                                    height: 94.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      image: DecorationImage(
                                        image: fill,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(-3.0, 419.0),
            child: SizedBox(
              width: 200.0,
              child: Text(
                'Yami\'s today menu',
                style: TextStyle(
                  fontFamily: 'AppleSDGothicNeoSB00',
                  fontSize: 20,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 742.0),
            child: Text(
              'Yami\'s magazine',
              style: TextStyle(
                fontFamily: 'AppleSDGothicNeoSB00',
                fontSize: 20,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(325.0, 745.0),
            child: Text(
              'view all',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 12,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
                decoration: TextDecoration.underline,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 81.0),
            child: Container(
              width: 375.0,
              height: 108.0,
              decoration: BoxDecoration(
                color: const Color(0xffffc810),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 623.0),
            child: Container(
              width: 375.0,
              height: 100.0,
              decoration: BoxDecoration(
                color: const Color(0xfffff3c9),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(161.0, 668.0),
            child: SizedBox(
              width: 54.0,
              child: Text(
                'YAMI',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 762.0),
            child: SpecificRectClip(
              rect: Rect.fromLTWH(0, 0, 670, 190),
              child: UnconstrainedBox(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 660,
                  height: 190,
                  child: GridView.count(
                    primary: false,
                    padding: EdgeInsets.all(0),
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 4,
                    childAspectRatio: 0.7894736842105263,
                    children: [
                      {},
                      {},
                      {},
                      {},
                    ].map((map) {
                      return Transform.translate(
                        offset: Offset(-16.0, -872.0),
                        child: Stack(
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(16.0, 872.0),
                              child: Container(
                                width: 150.0,
                                height: 190.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: const Color(0xffffffff),
                                  border: Border.all(
                                      width: 1.0,
                                      color: const Color(0xff707070)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(-42.0, 28.0),
            child:
                // Adobe XD layer: 'main_bnr_img01' (shape)
                Container(
              width: 154.0,
              height: 154.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/main_bnr_img01.jpg'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(126.0, 68.0),
            child: Container(
              width: 149.0,
              height: 40.0,
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(130.0, 62.67),
            child: Text.rich(
              TextSpan(
                style: TextStyle(
                  fontFamily: 'Arial',
                  fontSize: 29,
                  color: const Color(0xff707070),
                  height: 1.3793103448275863,
                ),
                children: [
                  TextSpan(
                    text: '첫 결제시,\n',
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  TextSpan(
                    text: '샐러드 쿠폰무료!',
                    style: TextStyle(
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 557.0),
            child: Component111(),
          ),
          Transform.translate(
            offset: Offset(16.0, 188.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 1.0,
                  ease: Curves.easeOut,
                  pageBuilder: () => Productthumbnailsample(),
                ),
              ],
              child:
                  // Adobe XD layer: 'main_btn_대지 1' (shape)
                  Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/icon/main_btn_d1.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(139.0, 188.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 1.0,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home2(),
                ),
              ],
              child:
                  // Adobe XD layer: 'main_btn-02' (shape)
                  Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/icon/main_btn-02.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(262.0, 188.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 1.0,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home2(),
                ),
              ],
              child:
                  // Adobe XD layer: 'main_btn-03' (shape)
                  Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/icon/main_btn-03.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 302.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 1.0,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home2(),
                ),
              ],
              child:
                  // Adobe XD layer: 'main_btn-04' (shape)
                  Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/icon/main_btn-04.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(139.0, 302.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 1.0,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home2(),
                ),
              ],
              child:
                  // Adobe XD layer: 'main_btn-05' (shape)
                  Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/icon/main_btn-05.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(262.0, 302.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushLeft,
                  duration: 1.0,
                  ease: Curves.easeOut,
                  pageBuilder: () => Home2(),
                ),
              ],
              child:
                  // Adobe XD layer: 'main_btn-06' (shape)
                  Container(
                width: 98.0,
                height: 98.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: const AssetImage('assets/icon/main_btn-06.jpg'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(121.0, 2.0),
            child: Component141(),
          ),
        ],
      ),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(25.0), // here the desired height
       child: AppBar(
        // title: Text('widget.title')
        iconTheme: new IconThemeData(color: Color(0xffffc810)),
        backgroundColor: Colors.blue.withOpacity(0.0), //You can make this transparent
        elevation: 0.0, //No shadowR
       )
      ),
      drawer: AppDrawer(),
    );
  }
}

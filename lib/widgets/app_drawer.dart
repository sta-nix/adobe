import 'package:flutter/material.dart';
import '../Screens/Welcome/welcome_screen.dart';
import '../home.dart';
import '../pages/messages.dart';
import '../pages/profile.dart';
import '../pages/settings.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Color(0xffffc810),
              ),
              child: Padding(
                padding: EdgeInsets.all(6),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width: 60, height: 60,
                      child: CircleAvatar(
                        backgroundImage: AssetImage('assets/image1.jpg'),
                      ),
                    ),
                    SizedBox(height: 16,),
                    Text("Mark Steve",style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),),
                    SizedBox(height: 3,),
                    Text("abc@gmail.com", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),),
                  ],
                ),
              ),
              // child: Text(
              //   'Drawer Header',
              //   style: TextStyle(
              //     color: Colors.white,
              //     fontSize: 24,
              //   )
              // ),
            ),
            Divider(
              height: 64,
              thickness: 0.5,
              color: Colors.white.withOpacity(0.3),
              indent: 32,
              endIndent: 32,
            ),            
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => home()));
              },
              leading: Icon(Icons.home),
              title: Text('Home'),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => Profile()));
              },
              leading: Icon(Icons.account_circle),
              title: Text('Profile'),
            ),
            Divider(
               height: 64,
               thickness: 0.5,
               color: Colors.white.withOpacity(0.3),
              indent: 32,
              endIndent: 32,
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => Settings()));
              },
              leading: Icon(Icons.settings),
              title: Text('Settings'),
            ),
           ListTile(
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => WelcomeScreen()));
              },
              leading: Icon(Icons.exit_to_app),
              title: Text('Logout'),
            ),            
          ]
        ),
      );
  }
}
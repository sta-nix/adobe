import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter_svg/flutter_svg.dart';

class Component71 extends StatelessWidget {
  Component71({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SvgPicture.string(
          _shapeSVG_30caa96e929344008fb9dcaeaeebe81a,
          allowDrawingOutsideViewBox: true,
        ),
      ],
    );
  }
}

const String _shapeSVG_30caa96e929344008fb9dcaeaeebe81a =
    '<svg viewBox="0.0 0.0 376.0 58.0" ><path  d="M 0 0 L 376 0 L 376 58 L 0 58 L 0 0 Z" fill="#fec82e" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './Detailpagesample.dart';

class Component41 extends StatelessWidget {
  Component41({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        PageLink(
          links: [
            PageLinkInfo(
              transition: LinkTransition.PushLeft,
              duration: 0.8,
              ease: Curves.easeInOutExpo,
              pageBuilder: () => Detailpagesample(),
            ),
          ],
          child:
              // Adobe XD layer: 'filipe-de-rodrigues…' (shape)
              Container(
            width: 375.0,
            height: 260.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage(''),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

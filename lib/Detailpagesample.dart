import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './Productthumbnailsample.dart';
import 'package:flutter_svg/flutter_svg.dart';
import './miguelmaldonadoqom5MPOERIunsplash.dart';
import 'package:adobe_xd/specific_rect_clip.dart';
import './Component51.dart';
import './Myshoppingbasket.dart';

class Detailpagesample extends StatelessWidget {
  Detailpagesample({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(0.0, 72.0),
            child:
                // Adobe XD layer: 'orlova-maria-oMTlhd…' (shape)
                Container(
              width: 375.0,
              height: 232.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/orlova-maria-oMTlhdFUhdI-unsplash.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 322.0),
            child: Text(
              'Store name',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 369.0),
            child: Text(
              'business hours',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(118.0, 369.0),
            child: Text(
              '10:00 ~ 22:30',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 399.0),
            child: Text(
              '514 Hansung giraffe Plaza, Jungang-daero, \nDong-gu, Busan',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(131.0, 322.0),
            child: Text(
              'bumildong',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(233.0, 322.0),
            child: Text(
              '0.1km',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(20.0, 20.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.PushRight,
                  duration: 0.3,
                  ease: Curves.linear,
                  pageBuilder: () => Productthumbnailsample(),
                ),
              ],
              child: Container(
                width: 30.0,
                height: 30.0,
                decoration: BoxDecoration(
                  color: const Color(0xffffffff),
                  border:
                      Border.all(width: 1.0, color: const Color(0xff707070)),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(130.0, 24.0),
            child: SizedBox(
              width: 116.0,
              child: Text(
                'detail page',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(14.5, 462.5),
            child: SvgPicture.string(
              _shapeSVG_a0e6de53be2443a9abcf9eab1d34c46a,
              allowDrawingOutsideViewBox: true,
            ),
          ),
          Transform.translate(
            offset: Offset(16.0, 509.0),
            child: SpecificRectClip(
              rect: Rect.fromLTWH(0, 0, 796, 94),
              child: UnconstrainedBox(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 795,
                  height: 94,
                  child: GridView.count(
                    primary: false,
                    padding: EdgeInsets.all(0),
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 5,
                    childAspectRatio: 1.5212765957446808,
                    children: [
                      {
                        'fill': const AssetImage('assets/images/miguel-maldonado-qom5MPOER-I-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                    ].map((map) {
                      final fill = map['fill'];
                      return Transform.translate(
                        offset: Offset(-16.0, -490.0),
                        child: Stack(
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(16.0, 490.0),
                              child:
                                  // Adobe XD layer: 'miguel-maldonado-qo…' (component)
                                  Stack(
                                children: <Widget>[
                                  // Adobe XD layer: 'emiliano-vittoriosi…' (shape)
                                  Container(
                                    width: 143.0,
                                    height: 94.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      image: DecorationImage(
                                        image: fill,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(-3.0, 488.0),
            child: SizedBox(
              width: 200.0,
              child: Text(
                'Yami\'s today menu',
                style: TextStyle(
                  fontFamily: 'AppleSDGothicNeoSB00',
                  fontSize: 20,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 69.0),
            child: Stack(
              children: <Widget>[
                Transform.translate(
                  offset: Offset(0.0, 539.0),
                  child: Component51(),
                ),
                Transform.translate(
                  offset: Offset(143.0, 560.0),
                  child: PageLink(
                    links: [
                      PageLinkInfo(
                        transition: LinkTransition.Fade,
                        duration: 0.3,
                        ease: Curves.easeOut,
                        pageBuilder: () => Myshoppingbasket(),
                      ),
                    ],
                    child: SizedBox(
                      width: 98.0,
                      child: Text(
                        '장바구니 담기',
                        style: TextStyle(
                          fontFamily: 'Apple SD Gothic Neo',
                          fontSize: 15,
                          color: const Color(0xff707070),
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

const String _shapeSVG_a0e6de53be2443a9abcf9eab1d34c46a =
    '<svg viewBox="14.5 462.5 348.0 1.0" ><path transform="translate(14.5, 462.5)" d="M 0 0 L 348 0" fill="none" stroke="#707070" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

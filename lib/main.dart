import 'package:adobe/search_bloc/bloc.dart';
import 'package:adobe/story_bloc/bloc.dart';
import 'package:adobe/talk_bloc/bloc.dart';
import 'package:adobe/user_bloc/bloc.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:adobe/route_generator.dart';
import 'package:kakao_flutter_sdk/auth.dart';
import 'package:bloc/bloc.dart';

import 'package:adobe/constants.dart';
import 'login.dart';
import 'add_story.dart';
import 'bloc_delegate.dart';
import 'intro.dart';
import 'link.dart';
// import 'root_page.dart';
import 'search.dart';
// import 'services/authentication.dart';
import 'story.dart';
import 'talk.dart';
import 'user.dart';
import 'Screens/splash_screen.dart';
import 'route_generator.dart';

// void main() => runApp(DevicePreview( builder: (context)=> MyApp(),),);//for pv
void main() {
  KakaoContext.clientId = "17fdc7e66e5ac53d5c4567fb7e52eff9";
  KakaoContext.javascriptClientId = "66aa7d2674a44c1acf3a5eb297b20ce9";

  BlocSupervisor.delegate = MyBlocDelegate();
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<UserBloc>(create: (context) => UserBloc()),
      BlocProvider<StoryBloc>(create: (context) => StoryBloc()),
      BlocProvider<TalkBloc>(create: (context) => TalkBloc()),
      BlocProvider<FriendsBloc>(create: (context) => FriendsBloc()),
      BlocProvider<StoryDetailBloc>(
        create: (context) => StoryDetailBloc(),
      ),
      BlocProvider<PostStoryBloc>(
        create: (context) => PostStoryBloc(),
      ),
      BlocProvider<SearchBloc>(create: (context) => SearchBloc())
    ],
    child: MyApp(),
  ));
  // runApp(MyApp());
}
class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
      _MyAppState createState() => _MyAppState();
//   Widget build(BuildContext context) {
//     return MaterialApp(
//     // builder: DevicePreview.appBuilder,// for pv
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Auth',
//       theme: ThemeData(
//         primaryColor: kPrimaryColor,
//         scaffoldBackgroundColor: Colors.white,
//       ),
//       home: SplashScreen(),
//     );
//   }
}
class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Kakao Flutter SDK Sample",
      initialRoute: "/",
      onGenerateRoute: RouteGenerator.generateRoute,
      // routes: {
      //   "/": (context) => SplashScreen(),
      //   "/main": (context) => MainScreen(),
      //   "/login": (context) => LoginScreen(),
      //   "/stories/post": (context) => AddStoryScreen(),
      //   // "/stories/detail": (context) => StoryDetailScreen()
      // },
    );
  }
}
class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    if (kIsWeb) {
      AuthCodeClient.instance.retrieveAuthCode();
    }
    _checkAccessToken();
  }

  _checkAccessToken() async {
    var token = await AccessTokenStore.instance.fromStore();
    debugPrint(token.toString());
    if (token.refreshToken == null) {
      Navigator.of(context).pushReplacementNamed('/login');
    } else {
      Navigator.of(context).pushReplacementNamed("/main");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainScreenState();
  }
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  int tabIndex = 0;
  TabController _controller;

  String _title = "User API";

  List<Widget> _actions = [];

  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserBloc>(context).add(UserFetchStarted());
    _controller = TabController(length: 4, vsync: this);
    _actions = _searchActions();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UserBloc, UserState>(
      listener: (context, state) {
        if (state is UserLoggedOut) {
          Navigator.of(context).pushReplacementNamed("/login");
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(_title),
          actions: _actions,
        ),
        body: TabBarView(
          controller: _controller,
          children: [UserScreen(), TalkScreen(), StoryScreen(), LinkScreen()],
        ),
        bottomNavigationBar: TabBar(
          controller: _controller,
          labelColor: Colors.black,
          tabs: [
            Tab(
              icon: Icon(Icons.ac_unit, color: Color.fromARGB(255, 0, 0, 0)),
              text: "User",
            ),
            Tab(
              icon: Icon(Icons.ac_unit, color: Color.fromARGB(255, 0, 0, 0)),
              text: "Talk",
            ),
            Tab(
              icon: Icon(Icons.ac_unit, color: Color.fromARGB(255, 0, 0, 0)),
              text: "Story",
            ),
            Tab(
              icon: Icon(Icons.ac_unit, color: Color.fromARGB(255, 0, 0, 0)),
              text: "Link",
            )
          ],
          onTap: setTabIndex,
        ),
      ),
    );
  }

  List<Widget> _searchActions() {
    return [
      IconButton(
          icon: Icon(Icons.search),
          onPressed: () {
            showSearch(
                context: context,
                delegate: DataSearch(BlocProvider.of<SearchBloc>(context)));
          })
    ];
  }

  List<Widget> _storyActions() {
    return [
      IconButton(
        icon: Icon(CupertinoIcons.add),
        onPressed: () => {
          Navigator.of(context).push(CupertinoPageRoute(
              fullscreenDialog: true, builder: (context) => AddStoryScreen()))
        },
      )
    ];
  }

  void setTabIndex(index) {
    String title;

    List<Widget> actions = _searchActions();
    switch (index) {
      case 0:
        title = "User API";
        BlocProvider.of<UserBloc>(context).add(UserFetchStarted());
        break;
      case 1:
        title = "Talk API";
        BlocProvider.of<TalkBloc>(context).add(FetchTalkProfile());
        BlocProvider.of<FriendsBloc>(context).add(FetchFriends());
        break;
      case 2:
        title = "Story API";
        BlocProvider.of<StoryBloc>(context).add(FetchStories());
        actions = _storyActions();
        break;
      case 3:
        title = "KakaoLink";
        break;
      default:
    }
    setState(() {
      tabIndex = index;
      _controller.index = tabIndex;
      _title = title;
      if (actions != null) {
        _actions = actions;
      }
    });
  }
}
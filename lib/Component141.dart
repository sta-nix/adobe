import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:adobe_xd/page_link.dart';
import './Addressset.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Component141 extends StatelessWidget {
  Component141({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Transform.translate(
          offset: Offset(-13.0, 0.0),
          child: PageLink(
            links: [
              PageLinkInfo(
                transition: LinkTransition.Fade,
                duration: 0.3,
                ease: Curves.easeOut,
                pageBuilder: () => Addressset(),
              ),
            ],
            child: SizedBox(
              width: 160.0,
              child: Text(
                'bumil street 514',
                style: TextStyle(
                  fontFamily: 'Apple SD Gothic Neo',
                  fontSize: 20,
                  color: const Color(0xff707070),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(141.0, 5.0),
          child: PageLink(
            links: [
              PageLinkInfo(
                transition: LinkTransition.SlideLeft,
                duration: 1.0,
                ease: Curves.easeOut,
                pageBuilder: () => Addressset(),
              ),
            ],
            child: SvgPicture.string(
              _shapeSVG_007e0a92ca344f35b7308793ed723e10,
              allowDrawingOutsideViewBox: true,
            ),
          ),
        ),
      ],
    );
  }
}

const String _shapeSVG_007e0a92ca344f35b7308793ed723e10 =
    '<svg viewBox="141.0 5.0 13.0 11.0" ><path transform="translate(141.0, 5.0)" d="M 5.639073371887207 1.456952571868896 C 6.026134967803955 0.8019249439239502 6.973864555358887 0.8019249439239502 7.360926628112793 1.456952571868896 L 12.10847759246826 9.491271018981934 C 12.50238800048828 10.15788650512695 12.02185153961182 11.00000095367432 11.24755191802979 11 L 1.752448320388794 11 C 0.9781482815742493 11.00000095367432 0.4976126253604889 10.15788650512695 0.8915217518806458 9.491271018981934 Z" fill="#fec82e" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import './Component121.dart';
import './Component21.dart';

class Component111 extends StatelessWidget {
  Component111({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: 375.0,
          height: 70.0,
          decoration: BoxDecoration(
            color: const Color(0xfffcfcfc),
          ),
        ),
        Transform.translate(
          offset: Offset(13.0, 0.0),
          child: Component121(),
        ),
        Transform.translate(
          offset: Offset(83.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffcf8d),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(83.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(153.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(223.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(293.0, 0.0),
          child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(34.0, 48.0),
          child: SizedBox(
            width: 28.0,
            child: Text(
              'home',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(102.0, 48.0),
          child: SizedBox(
            width: 32.0,
            child: Text(
              'search',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(168.0, 48.0),
          child: SizedBox(
            width: 40.0,
            child: Text(
              'location',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(242.0, 48.0),
          child: SizedBox(
            width: 32.0,
            child: Text(
              'basket',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(315.0, 48.0),
          child: SizedBox(
            width: 26.0,
            child: Text(
              'more',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 10,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(37.0, 14.0),
          child:
              // Adobe XD layer: 'navi_icon_대지 1' (shape)
              Container(
            width: 23.0,
            height: 23.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/icon/navi_icon_대지 1.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(105.0, 14.0),
          child:
              // Adobe XD layer: 'navi_icon-02' (shape)
              Container(
            width: 23.0,
            height: 23.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/icon/navi_icon-02.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(176.0, 14.0),
          child:
              // Adobe XD layer: 'navi_icon-03' (shape)
              Container(
            width: 23.0,
            height: 23.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/icon/navi_icon-03.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(247.0, 14.0),
          child:
              // Adobe XD layer: 'navi_icon_basket' (shape)
              Container(
            width: 23.0,
            height: 23.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/icon/navi_icon_basket.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(317.0, 14.0),
          child:
              // Adobe XD layer: 'navi_icon-05' (shape)
              Container(
            width: 23.0,
            height: 23.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/icon/navi_icon-05.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Transform.translate(
          offset: Offset(0.5, 0.5),
          child: Component21(),
        ),
      ],
    );
  }
}

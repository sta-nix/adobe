import 'package:adobe_xd/page_link.dart';
import 'package:adobe_xd/specific_rect_clip.dart';
import 'package:flutter/material.dart';
import 'Component21.dart';
import 'Myshoppingbasket.dart';
import 'classes/hero_type.dart';

class Details extends StatefulWidget {
  final HeroType heroType;

  const Details({Key key, this.heroType}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  HeroType _heroType;
  double _screenWidth;

  @override
  void initState() {
    super.initState();
    _heroType = widget.heroType;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _screenWidth = MediaQuery.of(context).size.width;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${_heroType.title}'),
        backgroundColor: _heroType.materialColor,
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Transform.translate(
            offset: Offset(16.0, 440.0),
            child: SpecificRectClip(
              rect: Rect.fromLTWH(0, 0, 796, 94),
              child: UnconstrainedBox(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 795,
                  height: 94,
                  child: GridView.count(
                    primary: false,
                    padding: EdgeInsets.all(0),
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    crossAxisCount: 5,
                    childAspectRatio: 1.5212765957446808,
                    children: [
                      {
                        'fill': const AssetImage('assets/images/miguel-maldonado-qom5MPOER-I-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                      {
                        'fill': const AssetImage('assets/images/filipe-de-rodrigues-SpMmd2jgL2w-unsplash.jpg'),
                      },
                    ].map((map) {
                      final fill = map['fill'];
                      return Transform.translate(
                        offset: Offset(-16.0, -490.0),
                        child: Stack(
                          children: <Widget>[
                            Transform.translate(
                              offset: Offset(16.0, 490.0),
                              child:
                                  // Adobe XD layer: 'miguel-maldonado-qo…' (component)
                                  Stack(
                                children: <Widget>[
                                  // Adobe XD layer: 'emiliano-vittoriosi…' (shape)
                                  Container(
                                    width: 143.0,
                                    height: 94.0,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      image: DecorationImage(
                                        image: fill,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
            Hero(
              tag: 'background' + _heroType.title,
              child: Container(
                color: Colors.transparent,//_heroType.materialColor,
              ),
            ),
            Positioned(
                top: 0.0,
                left: 0.0,
                width: _screenWidth,
                height: 230.0,
                child: Hero(
                    tag: 'image' + _heroType.title,
                    child: Image.asset(
                      _heroType.image,
                      fit: BoxFit.fitWidth,
                    ))),
            Positioned(
                top: 250.0,
                left: 32.0,
                width: _screenWidth - 64.0,
                child: Hero(
                    tag: 'text' + _heroType.title,
                    child: Material(
                        color: Colors.transparent,
                        child: Text(
                          '${_heroType.title}',
                          style: TextStyle(
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold,
                              color: _heroType.materialColor.shade900),
                        )))),
            Positioned(
                top: 280.0,
                left: 32.0,
                width: _screenWidth - 64.0,
                child: Hero(
                    tag: 'subtitle' + _heroType.title,
                    child: Material(
                        color: Colors.transparent,
                        child: Text(
                          _heroType.subTitle,
                        )))),
          Transform.translate(
            offset: Offset(14.0, 322.0),
            child: Text(
              'Store name',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(0.0, 539.0),
            child: Container(
              width: 376.0,
              height: 58.0,
              decoration: BoxDecoration(
                color: const Color(0xfffec82e),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(143.0, 560.0),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.Fade,
                  duration: 0.3,
                  ease: Curves.easeOut,
                  pageBuilder: () => Myshoppingbasket(),
                ),
              ],
              child: SizedBox(
                width: 98.0,
                child: Text(
                  '장바구니 담기',
                  style: TextStyle(
                    fontFamily: 'Apple SD Gothic Neo',
                    fontSize: 15,
                    color: const Color(0xff707070),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
                    Transform.translate(
            offset: Offset(14.0, 369.0),
            child: Text(
              'business hours',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(118.0, 369.0),
            child: Text(
              '10:00 ~ 22:30',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(14.0, 399.0),
            child: Text(
              'Address',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(78.0, 399.0),
            child: Text(
              '514 Hansung giraffe Plaza, Jungang-daero, \nDong-gu, Busan',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 14,
                color: const Color(0xff707070),
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(131.0, 322.0),
            child: Text(
              'bumildong',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(233.0, 322.0),
            child: Text(
              '0.1km',
              style: TextStyle(
                fontFamily: 'Apple SD Gothic Neo',
                fontSize: 20,
                color: const Color(0xff707070),
                fontWeight: FontWeight.w300,
              ),
              textAlign: TextAlign.left,
            ),
          ),
                    Stack(
            children: <Widget>[
              Transform.translate(
                offset: Offset(0.0, 597.0),
                child: Container(
                  width: 375.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xfffcfcfc),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(83.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffcf8d),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(83.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(153.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(223.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(293.0, 597.0),
                child: Container(
                  width: 70.0,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: const Color(0xffffffff),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(110.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(110.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(180.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(250.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(320.0, 612.0),
                child: Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                    color: const Color(0xffacacac),
                  ),
                ),
              ),
              Stack(
                children: <Widget>[
                  Transform.translate(
                    offset: Offset(13.0, 597.0),
                    child: Container(
                      width: 70.0,
                      height: 70.0,
                      decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(40.0, 612.0),
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.all(Radius.elliptical(8.0, 8.0)),
                        color: const Color(0xffacacac),
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(42.0, 645.0),
                    child: SizedBox(
                      width: 12.0,
                      child: Text(
                        '홈',
                        style: TextStyle(
                          fontFamily: 'Apple SD Gothic Neo',
                          fontSize: 10,
                          color: const Color(0xff707070),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              Transform.translate(
                offset: Offset(107.0, 645.0),
                child: SizedBox(
                  width: 22.0,
                  child: Text(
                    '검색',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(173.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '내주변',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(243.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '내정보',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(313.0, 645.0),
                child: SizedBox(
                  width: 30.0,
                  child: Text(
                    '더보기',
                    style: TextStyle(
                      fontFamily: 'Apple SD Gothic Neo',
                      fontSize: 10,
                      color: const Color(0xff707070),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(0.5, 597.5),
                child: Component21(),
              ),
            ],
          ),
          ],
        ),
      ),
    );
  }
}
